# Connect to Azure
$connectionName = "AzureRunAsConnection"
try
{
    # Get the connection "AzureRunAsConnection "
    $servicePrincipalConnection = Get-AutomationConnection -Name $connectionName         

    "Logging in to Azure..."
    Add-AzureRmAccount `
        -ServicePrincipal `
        -TenantId $servicePrincipalConnection.TenantId `
        -ApplicationId $servicePrincipalConnection.ApplicationId `
        -CertificateThumbprint   $servicePrincipalConnection.CertificateThumbprint 
}
catch {
    if (!$servicePrincipalConnection)
    {
        $ErrorMessage = "Connection $connectionName not found."
        throw $ErrorMessage
    } else{
        Write-Error -Message $_.Exception
        throw $_.Exception
    }
}


# Variables and Stuff
$vmCreds = Get-AutomationPSCredential -Name 'hybrid-worker-creds'
$rgName= 'test-aa-hw'
$vmName="hwmachine01"
$vmSize="Standard_B2ms"
$pubName="MicrosoftWindowsServer"
$offerName="WindowsServer"
$skuName="2016-Datacenter"
$OSDiskName = $vmName + "-osdisk"
$locName = "east us 2"

# Create Resource Group for VM
$rgDeploy = New-AzureRmResourceGroup -Name $rgName -Location $locName

# Get vNet Info
$vnet = Get-AzureRMVirtualNetwork -Name "azure-automation-vnet" -ResourceGroupName 'azure-automation'
$subnet = Get-AzureRmVirtualNetworkSubnetConfig -Name 'azure-automation-subnet' -VirtualNetwork $vnet
$pip = New-AzureRmPublicIpAddress -Name "hwmachine01-pip" -ResourceGroupName $rgName -Location $locName -AllocationMethod Dynamic
$nic = New-AzureRmNetworkInterface -Name "hwmachine01-nic" -ResourceGroupName $rgName -Location $locName -Subnet $subnet -PublicIpAddress $pip

# Create Tag(s)
$buildTags = @{
    Build_Date="$date";
    Project="Hybrid Worker Demo";
    Owner="Cloud Team";
    Platform="Windows";
    Environment="Test"
}

### Deploy VM
write-output "Creating Hybrid-Worker VM, please wait"

# Build out New VMConfig:
$vmconfig = New-AzureRmVMConfig -VMName $vmName -VMSize $VMSize -WarningAction silentlyContinue
$vmconfig = Set-AzureRmVMOperatingSystem -VM $vmconfig -ComputerName $VMName -Windows -Credential $vmCreds -ProvisionVMAgent:$true -WarningAction silentlyContinue
$vmconfig = Set-AzureRmVMSourceImage -VM $vmconfig -PublisherName "MicrosoftWindowsServer" -Offer "WindowsServer" -Skus "2016-Datacenter" -Version "latest" -WarningAction silentlyContinue
$vmconfig = Add-AzureRmVMNetworkInterface -VM $vmconfig -Id $nic.Id -WarningAction silentlyContinue
$vmconfig = Set-AzureRmVMBootDiagnostics -vm $vmconfig -Disable -WarningAction silentlyContinue
$vmconfig = Set-AzureRmVMOSDisk -VM $vmconfig -Name $OSDiskName -CreateOption FromImage -DiskSizeInGB 127 -Windows -WarningAction silentlyContinue

# Deploy Hybrid Worker VM with CustomSCriptExtension
New-AzureRmVM -ResourceGroupName $rgName -Location $locName -VM $vmconfig -tag $buildTags -DisableBginfoExtension -WarningAction silentlyContinue
Set-AzureRmVMCustomScriptExtension -ResourceGroupName $rgName -VMName $VMName -Location $locName -FileUri 'https://hwteststore.blob.core.windows.net/scripts/install-hybridworker.ps1' -Run 'install-hybridworker.ps1' -Name 'InstallHybridWorker'
