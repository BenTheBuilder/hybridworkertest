# Set Session\Environment to run all scripts unrestricted
Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force

# Create target folder
New-Item -Type Directory -Path 'c:\test'

# Download required files/scripts to promote server to Hybrid Worker and join to Automation Account
Invoke-WebRequest -Uri 'https://hwteststore.blob.core.windows.net/scripts/aa-context-file.json' -OutFile 'c:\test\aa-context-file.json'
Invoke-WebRequest -Uri 'https://hwteststore.blob.core.windows.net/scripts/New-OnPremiseHybridWorkerCustom.ps1' -OutFile 'c:\test\New-OnPremiseHybridWorkerCustom.ps1'

# Install NuGet v 2.8.5.208 required by Module(s)
Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force

# Run the Custom Hybrid Worker Script
c:\test\New-OnPremiseHybridWorkerCustom.ps1 `
        -AutomationAccountName  "test-azure-automation" `
        -AAResourceGroupName "azure-automation" `
        -OMSResourceGroupName "azure-automation" `
        -HybridGroupName "aa-hybridworker-group" `
        -SubscriptionId "ac7eb067-f411-480e-b1fb-d203797e7bf1" `
        -WorkspaceName "aa-ms"

