# Deploy Azure Automation Hybrid Worker via Runbook

## Create Context file for authentication

- Login to Azure with RunAsAccount created for AzureAutomation - you can use any account but this one is convient enough.

- Output the Azure Context file for the an AAD account using the following command.
```powershell
Save-AzureRmContext -Profile (Connect-AzureRmAccount) -Path c:\users\benmi\work\aa-context-file.json
```

## Steps needed to perform

- Create Credentials for Hybrid Worker VM login called hybrid-worker-creds.
- Create install-hybridworker.ps1 file with following:

```powershell
mkdir c:\test

iwr -Uri 'https://<storageAccountPath>/aa-conext-file.json' -OutFile 'c:\test\aa-context-file.json'
iwr -Uri 'https://<storageAccountPath>/New-OnPremiseHybridWorkerCustom.ps1' -OutFile 'c:\test\New-OnPremiseHybridWorkerCustom.ps1'

c:\test\New-OnPremiseHybridWorkerCustom.ps1 `
        -AutomationAccountName  "test-azure-automation" `
        -AAResourceGroupName "azure-automation" `
        -OMSResourceGroupName "azure-automation" `
        -HybridGroupName "aa-hybridworker-group" `
        -SubscriptionId "ac7eb067-f411-480e-b1fb-d203797e7bf1" `
        -WorkspaceName "aa-ms" 
```

## Trouble-shooting if something has gone terribly wrong

- If the scripts don't execute on the new vm (Set-AzureRmVMCustomScriptExtension error). You can start by running the Extension Diagnostics command or using the Portal under the VM - Extensions - View detailed status. 
- The script will be deployed on the server to this location "C:\Packages\Plugins\Microsoft.Compute.CustomScriptExtension\1.9.1"

```powershell
PS Azure:\> Get-AzureRmVMDiagnosticsExtension -ResourceGroupName 'test-aa-hw' -VMName 'hwmachine01' -Name 'InstallHybridWorker' -status


ResourceGroupName       : test-aa-hw
VMName                  : hwmachine01
Name                    : InstallHybridWorker
Location                : eastus2
Etag                    : null
Publisher               : Microsoft.Compute
ExtensionType           : CustomScriptExtension
TypeHandlerVersion      : 1.4
Id                      : /subscriptions/ac7eb067-f411-480e-b1fb-d203797e7bf1/resourceGroups/test-aa-hw/providers/Microsoft.Compute/virtualMachines/hwmachine01/extensions/InstallHybridWorker
PublicSettings          : {
                            "fileUris": [
                              "https://hwteststore.blob.core.windows.net/scripts/install-hybridworker.ps1"
                            ],
                            "commandToExecute": "powershell -ExecutionPolicy Unrestricted -file install-hybridworker.ps1 "
                          }
ProtectedSettings       :
ProvisioningState       : Succeeded
Statuses                : {Microsoft.Azure.Management.Compute.Models.InstanceViewStatus}
SubStatuses             : {Microsoft.Azure.Management.Compute.Models.InstanceViewStatus, Microsoft.Azure.Management.Compute.Models.InstanceViewStatus}
AutoUpgradeMinorVersion : True
ForceUpdateTag          :
```

## Invoke with Webhook
```powershell
invoke-restmethod -Method POST -Uri https://s1events.azure-automation.net/webhooks?token=EWcYQb8AqXYGbvvNO2sEVgSSdogVud9FIyUb4%2fXTG%2bY%3d
```